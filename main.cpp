#include <iostream>
#include <server/http_server.h>
#include "log/logger_main.h"
#include "setting/local_setting.h"
#include "signal.h"

//默认读写一个关闭的socket会触发sigpipe信号 该信号的默认操作是关闭进程 有时候这明显是我们不想要的
//所以此时我们需要重新设置sigpipe的信号回调操作函数 比如忽略操作等 使得我们可以防止调用它的默认操作
//信号的处理是异步操作 也就是说 在这一条语句以后继续往下执行中如果碰到信号依旧会调用信号的回调处理函数
//处理sigpipe信号
void handle_for_sigpipe() {

    struct sigaction sa; //信号处理结构体
    memset(&sa, '\0', sizeof(sa));
    sa.sa_handler = SIG_IGN;//设置信号的处理回调函数 这个SIG_IGN宏代表的操作就是忽略该信号
    sa.sa_flags = 0;
    if (sigaction(SIGPIPE, &sa, NULL))//将信号和信号的处理结构体绑定
        return;
}

int main() {
    handle_for_sigpipe();
    logger_init_async("./server.txt");
    LOG_INFO("服务器准备启动{}", 1);
    local_setting::get_instance().setPath("/home/hjj/study/okadmin");
    http_server s(8080);
    s.start();

    return 0;
}
