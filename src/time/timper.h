#pragma once
#include "priority_queue.h"
#include "time_stamp.h"
#include "unordered_map"
#include "mutex"

class timper {
private:

    struct Cache{
        int64_t time;
        int64_t fd;//socket
        bool operator==(const Cache& c) const
        {
          return  this->fd==c.fd;
        }
        bool operator<(const Cache& c) const
        {
            return  this->fd<c.fd;
        }
    };
    struct hash_key
    {
        std::size_t operator()(const Cache &key) const
        {
            using std::size_t;
            using std::hash;

            return (hash<int>()(key.fd));

        }
    };
    struct grater{
        bool operator()(const Cache& x,const Cache& y)
        {
            return x.time>y.time;
        }
    };

public:
    void add_timer(int64_t fd,int64_t t);
    void extern_time(int64_t fd);
    std::vector<int64_t> check_timer();

private:
    std::recursive_mutex mutex;
    //需要把时间映射到优先级队列
    hj::priority_queue<timper::Cache, grater, hash_key> heap;
};


