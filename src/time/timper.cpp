//
// Created by Administrator on 2023/3/14.
//
#include "timper.h"

//更新后的时间t
void timper::add_timer(int64_t  fd,int64_t t) {
    std::unique_lock<std::recursive_mutex> lock(mutex);
    timper::Cache c{t, fd};
   heap.push(c);
}

std::vector<int64_t> timper::check_timer() {

    std::unique_lock<std::recursive_mutex> lock(mutex);
    std::vector<int64_t> res;
    while (!heap.empty()) {
        if (heap.top().time < Timestamp::now().microSecondsSinceEpoch()) {
            auto t = heap.top();
            heap.pop();

            res.push_back(t.fd);
        } else {
            break;
        }
    }
    return res;
}

void timper::extern_time(int64_t fd) {
    std::unique_lock<std::recursive_mutex> lock(mutex);
    //utex.lock();
    //我现在特别怀疑我这个函数是不是写的有问题 .为什么一直崩溃 应该是安全的啊
    int index = heap.get_index(Cache{0, fd});
    auto t = addTime(Timestamp::now(), 5).microSecondsSinceEpoch();
    heap.container_modify(index, Cache{t, fd});
    // heap.get_container()[index]=Cache{t,fd}; //这边赋值的时候应该会死不安全的
    heap.adjustDown(index);//因为加时间了所以得需要向下调整
}
