#pragma once
#include <sys/epoll.h>
#include "thread_pool.h"
#include "memory"
#include "net/socket.h"
#include "http/http_connction_holder.h"
#include "time/timper.h"
class http_server {
public:
    http_server(int16_t port);

    void start();

    void event_handler(epoll_event &e);

    void loop();

    void do_acceptor();

private:
    std::unique_ptr<socket> socket_lis;//监听文件标识符
    int dum_fd;//预留空文件描述符
    int16_t port;//端口
    ThreadPool threadPool;
    http_connction_holder holder;
    timper t;
    std::mutex mutex;
};


