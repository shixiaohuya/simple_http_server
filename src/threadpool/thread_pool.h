#ifndef WEBSERVER_THREADPOOL_H
#define WEBSERVER_THREADPOOL_H

#include <atomic>
#include <condition_variable>
#include <functional>
#include <mutex>
#include <queue>
#include <thread>
#include <vector>
#include "noncopyable.h"

class ThreadPool : noncopyable {
public:
    using ThreadTask = std::function<void()>;

    ThreadPool();

    ~ThreadPool();

    void setThreadNum(int threadNum = std::thread::hardware_concurrency());

    void start();

    void stop();

    void addTask(ThreadTask task);

    size_t queueSize() const;

private:
    void threadPoolRunFunc();

    std::vector<std::unique_ptr<std::thread>> threads_;
    int threadNum_;

    mutable std::mutex mutex_;
    std::condition_variable notEmpty_;
    std::queue<ThreadTask> tasks_;
    std::atomic_bool running_;
};

#endif