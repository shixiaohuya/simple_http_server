#pragma once

/**
* A tag class emphasises the objects are non-copyable.
* A derived class should be a reference type rather than a value type.
*/
class noncopyable
{
public:
    noncopyable(const noncopyable&) = delete;
    void operator=(const noncopyable&) = delete;

protected:
    noncopyable() = default;
    ~noncopyable() = default;
};
