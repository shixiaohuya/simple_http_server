#pragma once
#include "vector"
#include "unistd.h"
#include "sys/epoll.h"
#include "noncopyable.h"

class e_poller:public noncopyable{
public:
    u_int32_t model=EPOLLET|EPOLLONESHOT;//默认模式et
    //返回值：成功时，epoll_ctl（）返回零。发生错误时，epoll_ctl（）返回-1并正确设置了errno。
    bool add_fd_ori(int fd,u_int32_t event);

    bool add_fd(int fd,u_int32_t event);
    bool update_fd(int fd,u_int32_t event);
    bool del_fd(int fd);
    int epoll_wait(int time=-1);
    virtual ~e_poller();
    static e_poller&  get_instance();
    epoll_event operator[](unsigned long long index);

private:
    e_poller();
    std::vector<epoll_event> container;
    int root_fd;

};


