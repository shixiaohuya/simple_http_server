#pragma once
#include "noncopyable.h"
#include "string"
class socket : public noncopyable{
public:
    socket();

    socket(int fd);
    socket &operator=(socket&&other);
    socket (socket&& s);
    void close();

    void bind(int16_t port = 8080);

    virtual ~socket();

    void listen();

    void set_reused(bool on = true);

    class socket accept(int);

    //todo 创建监听socket
    const int get_fd();

private:
    int fd;
    std::string remote_ip;
public:
    const std::string &getRemoteIp() const;

    void setRemoteIp(const std::string &remoteIp);


};


