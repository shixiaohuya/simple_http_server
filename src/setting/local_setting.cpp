//
// Created by Administrator on 2023/3/21.
//

#include "local_setting.h"

local_setting &local_setting::get_instance() {
    static local_setting l;
    return l;
}

const std::string &local_setting::getPath() const {
    return path;
}

void local_setting::setPath(const std::string& path) {
    get_instance().path=path;
}
