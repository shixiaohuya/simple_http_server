//
// Created by Administrator on 2023/3/21.
//
#pragma once
#include "noncopyable.h"
#include "string"
class local_setting: public noncopyable{

public:
    static local_setting &get_instance();

private:
   std::string  path;
public:
    const std::string &getPath() const;

    void setPath(const std::string& path);
};


