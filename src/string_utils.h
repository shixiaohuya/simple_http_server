#pragma once
#include "string"

inline std::string TrimLeft(const std::string &s, const char *t="\n\r\t") {
    size_t startpos = s.find_first_not_of(t);
    return (startpos == std::string::npos) ? "" : s.substr(startpos);
}

inline std::string TrimRight(const std::string &s, const char *t="\n\r\t") {
    size_t endpos = s.find_last_not_of(t);
    return (endpos == std::string::npos) ? "" : s.substr(0, endpos + 1);
}

inline std::string Trim(const std::string &s, const char *t="\n\r\t") {
    return TrimRight(TrimLeft(s, t), t);
}

