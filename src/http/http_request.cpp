//
// Created by Administrator on 2023/3/24.
//

#include "http_request.h"

httpparser::HttpRequestParser::ParseResult http_request::prase(const char *begin, const char *end) {
    return httpRequestParser.parse(this->request_result, begin, end);
}

void http_request::reset() {
    this->request_result.method.clear();
    this->request_result.content.clear();
    this->request_result.headers.clear();
    this->request_result.uri.clear();
    this->request_result.keepAlive = false;
    this->request_result.versionMajor = 0;
}

std::string http_request::get_etag() {

    for (const auto &item : this->request_result.headers) {
        if (item.name.compare("If-None-Match") == 0)
            return item.value;

    }

    return "";
}
