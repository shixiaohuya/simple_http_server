//
// Created by Administrator on 2023/3/17.
//
#pragma once
#include "unordered_map"
#include "memory"
#include "shared_mutex"
class http_connection;
class http_connction_holder {
public:
    std::shared_ptr<http_connection> get_connection_by_fd(int fd);
    void add(int fd,std::shared_ptr<http_connection> con );
    void closed_by_fd(int fd);

private:
    std::shared_mutex mutex;
    std::unordered_map<int,std::shared_ptr<http_connection>>  mp;


};

