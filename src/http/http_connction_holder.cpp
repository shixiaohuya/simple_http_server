//
// Created by Administrator on 2023/3/17.
//

#include <mutex>
#include <net/e_poller.h>
#include "http_connction_holder.h"
#include "http_connection.h"
#include "log/logger_main.h"
//这边本来打算用读写锁的 但是c++的锁不可重入 写锁会造成死锁
std::shared_ptr<http_connection> http_connction_holder::get_connection_by_fd(int fd) {
    std::shared_lock<std::shared_mutex> lock(mutex);//加共享锁 里面必须是sharad_mute
    return this->mp[fd];
}

void http_connction_holder::add(int fd, std::shared_ptr<http_connection> con) {
    std::unique_lock<std::shared_mutex> lock(mutex);//加独占锁里面不需要是shared_mutex mutex 都行
    this->mp[fd]=con;
}

void http_connction_holder::closed_by_fd(int fd) {
    std::unique_lock<std::shared_mutex> lock(mutex);
    auto iter=this->mp.find(fd);
    if (iter!=mp.end())
    {


        iter->second->set_close_flag(true);//设置关闭
        //关闭连接
        e_poller::get_instance().del_fd(fd);//删除红黑树
        ::close(fd);//关闭连接符
        this->mp.erase(fd);//移除链接holder
        LOG_INFO("http_connection::closed()", fd);
    }

}

