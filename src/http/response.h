//
// Created by Administrator on 2023/3/18.
//
#pragma once

#include <constant/constant.h>
#include "string"
#include "http_request.h"
#include "buffer/buffer.h"

class response {
public:
    bool flag = false;

    response(http_request &req, std::string &buffer_write);//初始化一些内容
    void make_response();//
    void must_init();

    void make_bad_response();

    std::string real_path;
    std::string file_type;

    void reset();

private:
    http_request &req_;
    std::string &buffer_;
    std::string request_etag;

    std::string get_file_etag();

    std::string get_file_type_init();

    void make_response_head(int num = 0);

    void make_response_body();

    std::string state_to_string(status_type type);

};


