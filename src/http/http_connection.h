//
// Created by Administrator on 2023/3/17.
//
#pragma once
#include "net/socket.h"
#include "http/http_connction_holder.h"
#include "buffer/buffer.h"
#include "response.h"
#include "http_request.h"

class http_connection: public std::enable_shared_from_this<http_connection>{

public:
    void closed();
    void do_read();
    void do_write();

    void do_write_more();

    void start();

    ssize_t wirte_buff(int &outError);

    void set_close_flag(bool t);

    virtual ~http_connection();

    http_connection(http_connction_holder &holder, class socket socket);

private:
    http_connction_holder &holder;

    class socket socket_;

    bool is_close = false;
    buffer buffer_read;
    std::string buffer_write;
    http_request request_;
    // request request_;
    response response_{request_, buffer_write};

};


