#pragma once

#include "util/request_util.h"
#include "util/httprequestparser.h"

class http_request {
public:
    httpparser::Request request_result;//解析结果
    httpparser::HttpRequestParser::ParseResult prase(const char *begin, const char *end);

    std::string get_etag();

    void reset();

private:
    httpparser::HttpRequestParser httpRequestParser;


};


