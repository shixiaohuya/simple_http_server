#pragma once
#include "h_log.h"

static  void logger_init_async(std::string path)
{
    static Hlog log;
    log.Init(path.data(), 1024*1024*1000, 10,
             Hlog::ASYNC, Hlog::CONSOLE_AND_FILE, Hlog::LEVEL_DEBUG);
    LOG_INFO("初始化日志成功....");
//LOG_TRACE("test {}", 1);
//LOG_DEBUG("test {:.2f}", 1.0000);
//LOG_INFO("test {}", 1.23456789);
//LOG_WARN("test {}", 'A');
//LOG_ERROR("test {}", "ABC");
//LOG_CRITI("test {}", std::string("abc"));


}
